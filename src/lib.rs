use anyhow;
pub mod config;

pub fn setup(config: config::Config) -> Result<(), anyhow::Error> {
    // doesn't erase my regular todos
    let todo_file = utils::create_open_file(&config.files_location, "todo.txt")?;
    let done_file = utils::create_open_file(&config.files_location, "done.txt")?;
    Ok(())
}

mod utils {
    use std::fs::{create_dir_all, File, OpenOptions};
    use std::path::PathBuf;
    pub fn create_open_file(basepath: &PathBuf, filename: &str) -> Result<File, anyhow::Error> {
        let file_path = basepath.join(filename);
        create_dir_all(basepath)?;
        Ok(OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(file_path)?)
    }
}
