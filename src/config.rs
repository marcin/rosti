use anyhow;
use confy;
use directories::BaseDirs;
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub files_location: PathBuf,
}

impl Default for Config {
    fn default() -> Self {
        // I know that this should propagate error to some nice user thingie, but it's suuper
        // unlikely to actually fail
        let file_dir = BaseDirs::new()
            .expect("Couldn't locate home directory while generating new config")
            .home_dir()
            .join("todo");
        Self {
            files_location: file_dir,
        }
    }
}

pub fn get_config(path: Option<PathBuf>) -> anyhow::Result<Config> {
    let config = match path {
        Some(path) => confy::load_path(path)?,
        None => confy::load("rosti")?,
    };
    Ok(config)
}
