use rosti;
use std::path::{Path, PathBuf};

fn main() -> Result<(), anyhow::Error> {
    let config = rosti::config::get_config(Some(PathBuf::from("../tests/test_config.toml")))?;
    // let config = get_config(None)?; -- keeping it this way until more tested to make sure it
    rosti::setup(config);
    Ok(())
}
