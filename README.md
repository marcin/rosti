# Intro

A [todo.txt](http://todotxt.org/) client which uses [Rofi](https://github.com/davatorium/rofi) as a frontend.

# But what about todofi.sh?

This is indeed a rewrite of [todofi.sh](https://github.com/hugokernel/todofi.sh) in
Rust, with some minor improvements. It's much faster, there's no UI lag. And
it's got keyboard shortcuts.
